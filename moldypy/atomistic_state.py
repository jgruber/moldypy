import numpy as np
import pandas as pd
import ctypes
import _moldypy

class DumpFormatError(Exception):
  pass

class SimulationCell:
  def __init__(self, matrix=[[0.0,1.0], [0.0,1.0], [0.0,1.0]], 
    boundary=[True, True, True],
    tilt=[0.0, 0.0, 0.0]):
    self.matrix = np.array(matrix) 
    self.boundary = np.array(boundary, dtype="int32")
    self.tilt = np.array(tilt)
    self.l = np.diff(matrix).T[0]
    self.is_triclinic = (self.tilt!=0.0).any()

  def __str__(self):
    return "".join([
      "<Triclinic %s" % str(self.tilt) if self.is_triclinic else "<Ortho ",
      str(self.matrix[:,0]),
      " to ",
      str(self.matrix[:,1]),
      " ",
      str(self.boundary), 
      ">",
    ])

  def __repr__(self):
    return str(self)


class AtomisticState:
  def __init__(self, atoms, meta, box):
    self.atoms = atoms
    self.positions = self.atoms[ ['x','y','z'] ].values
    self.meta = meta
    self.box = box
    self.dump_info_capsule = None

  def __str__(self):
    return "<AtomisticState>"

  def __repr__(self):
    return "<AtomisticState>"

  @staticmethod
  def read_lammps_dump(filename):
    with open(filename) as f:
      lines = iter(f.readlines())

    meta = {}
    atoms = []
    box = SimulationCell()

    for l in lines:
      if "ITEM: TIMESTEP" in l:
        meta['timestep'] = int( lines.next() )

      elif "ITEM: NUMBER OF ATOMS" in l:
        meta['n_atoms'] = int( lines.next() )

      elif "ITEM: BOX BOUNDS" in l:
        if "xy xz yz" in l:
          raise MoldypyUnsupportedError("Triclinic Boxes not yet supported by read_lammps_dump")
        else:
          boundary = [ b=='p' or b=='pp' for b in l[17:].split() ]
          matrix = np.array([ map(float,lines.next().split()) for i in range(3) ])
          box = SimulationCell(matrix, boundary)

      elif "ITEM: ATOMS" in l:
        columns = l[12:].split()
        try:
          n = meta['n_atoms']
        except KeyError:
          raise DumpFormatError("Invalid Dump File: 'ITEM: NUMBER OF ATOMS' must precede 'ITEM: ATOMS'")

        atoms = np.zeros( (meta['n_atoms'], len(columns)) )
        for i in range(n):
          atoms[i] = map(float, lines.next().split())
      else:
        raise DumpFormatError("Invalid Dump File: Unexpected line \"%s\" " % l)

    atoms_df = pd.DataFrame(atoms, columns=columns)
    return AtomisticState(atoms_df, meta, box)

  def read_lammps_data(self, filename):
    pass

  def write_lammps_dump(self, filename):
    pass

  def write_lammps_data(self, filename):
    pass

  def load_dump_info(self):
    self.dump_info_capsule = _moldypy.init_dump_info(
        self.positions,
        self.box.matrix,
        self.box.boundary,
      )    
    print self.positions[-1]
    return self.dump_info_capsule


  def calculate_nearest_neighbor_list(self, cutoff):
    if self.dump_info_capsule == None:
      self.load_dump_info()

    nnl = _moldypy.nnl_pylist(self.dump_info_capsule, cutoff)

    return nnl




















