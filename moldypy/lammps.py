import numpy as np
from bunch import Bunch
import traceback, sys

class LammpsDumpError(Exception):
  """Error singifying incorrectly formatted dump."""
  def __init__(self, message, cause):
    super(LammpsDumpError, self).__init__("%s %s" % (message, repr(cause)))

class DumpFormatError(Exception):
  """Error singifying incorrectly formatted dump."""
  pass

class DumpArgError(Exception):
  """Error singifying incorrect arguments to write_dump."""
  pass

ITEM_TIMESTEP = "ITEM: TIMESTEP"
ITEM_NATOMS = "ITEM: NUMBER OF ATOMS"
ITEM_BOX = "ITEM: BOX BOUNDS"
ITEM_ATOMS = "ITEM: ATOMS"
TRICLINIC_TAG = "xy xz yz"

def read_data(filename):
  pass

def write_data(filename):
  pass

def read_dump(filename):
  """Read a LAMMPS Dump File.

  returns: (atoms, meta)
    atoms: numpy ndarry containing particle data
    meta: dictionary containing the timestep, number of atoms, simulation cell and column headers.
  """
  with open(filename) as f:
    lines = iter(f.readlines())

  meta = Bunch()
  atoms = []

  for l in lines:
    if ITEM_TIMESTEP in l:
      meta['timestep'] = int( lines.next() )

    elif ITEM_NATOMS in l:
      meta['n_atoms'] = int( lines.next() )

    elif ITEM_BOX in l:
      boxstr = l.split(ITEM_BOX,1)[1]
      bounds = boxstr.split()[-3:]
      box = np.array([ map(float,lines.next().split()) for p in bounds ])
      meta['box'] = box, bounds

    elif ITEM_ATOMS in l:
      meta['columns'] = l.split(ITEM_ATOMS,1)[1].split()
      try:
        atoms = np.zeros( (meta['n_atoms'], len(meta['columns'])) )
      except KeyError:
        raise DumpFormatError("Invalid Dump File: 'ITEM: NUMBER OF ATOMS' must precede 'ITEM: ATOMS'")

      for i in range(meta['n_atoms']):
        atoms[i] = map(float, lines.next().split())
    else:
      raise DumpFormatError("Invalid Dump File: Unexpected line \"%s\" " % l)

  return atoms, meta


def load_dump(filename):
  """Load a LAMMPS dump file

  return varies based on number of dumps in file:
  0 -> None
  1 -> Bunch containing the dump info
  >1 -> List of bunches
  """
  snaps = []
  with open(filename) as lines:
    try:
      while 1:
        snaps.append(parse_snapshot(lines))
    except StopIteration:
      pass
    except Exception as e:
      raise LammpsDumpError, ("Load %s Failed:"%filename, e), sys.exc_info()[2]

  if len(snaps) == 0:
    return None
  elif len(snaps) == 1:
    return snaps[0]
  else:
    return snaps


def parse_snapshot(lines):
  """Parse a snapshot from a LAMMPS dump file into a bunch of usable data.

  This function is intended to fail if the dump is not properly formatted.
  It probably accepts some invalid dumps, but what can you do...
  """
  dump = Bunch()
  assert(ITEM_TIMESTEP in lines.next())
  dump.timestep = int(lines.next())
  assert(ITEM_NATOMS in lines.next())
  dump.n_atoms = int(lines.next())
  dump.boxstr = lines.next().split(ITEM_BOX,1)[1].strip()
  dump.bounds = dump.boxstr.split()[-3:]
  dump.box = np.array([ map(float,lines.next().split()) for p in dump.bounds ])
  dump.columns = lines.next().split(ITEM_ATOMS,1)[1].split()
  dump.atoms = np.zeros( (dump.n_atoms, len(dump.columns)) )
  for i in xrange(dump.n_atoms):
    dump.atoms[i] = map(float, lines.next().split())
  return dump

def export_dump(filename, dump):
  try:
    with open(filename,"w") as f:
      f.write("%s\n%d\n" % (ITEM_TIMESTEP, dump.timestep))
      f.write("%s\n%d\n" % (ITEM_NATOMS, dump.n_atoms))
      boxtok = ([TRICLINIC_TAG] if dump.box.shape==(3,3) else []) + dump.bounds
      boxstr = " ".join(boxtok)
      f.write("%s %s\n" % (ITEM_BOX, boxstr))
      np.savetxt(f, dump.box)
      f.write("%s %s\n" % (ITEM_ATOMS, " ".join(dump.columns)) )
      np.savetxt(f, dump.atoms)
  except Exception as e:
    raise LammpsDumpError, ("Export Failed:", e), sys.exc_info()[2]

def write_dump(filename, atoms, meta={}, **kwargs):
  """ Write a LAMMPS Dump File

  # timestep, box and columns can be defined in the meta dictionary or directly as arguments
  # arguments specified directly take precedence over those defined in meta
  # expected formats
  # filename: string
  # atoms: mxn array-like
  # timestep: non-negative integer
  # box: tuple of (3x2 float array-like, list of 3 strings)
  # columns: list of strings with length equal to width of atoms array (n)
  """

  #figure out where our data is
  def find_value(name):
    if name in kwargs:
      return kwargs[name]
    elif name in meta:
      return meta[name]
    else:
      raise DumpArgError("Parameter '%s' must be defined!" % name)

  timestep, box, columns = map(find_value, ["timestep","box","columns"])

  #basic sanity checking
  if len(atoms) > 0 and len(columns) != len(atoms[0]):
    raise DumpArgError("length of column headers (%d) does not match atom entries (%d)" % (len(columns),len(atoms[0])))

  with open(filename,'w') as f:
    f.write("ITEM: TIMESTEP\n%d\n" % timestep)
    f.write("ITEM: NUMBER OF ATOMS\n%d\n" % len(atoms))
    f.write("ITEM: BOX BOUNDS %s %s %s\n" % tuple(box[1][:3]))
    for axis in box[0]:
      f.write("%f %f\n" % tuple(axis[:2]))

    f.write("ITEM: ATOMS %s\n" % " ".join(columns))
    for atom in atoms:
      atom = list(atom)
      for i,c in enumerate(columns):
        if c in ['id','type','c_cna']:
          atom[i] = int(atom[i])
      f.write("%s\n" % " ".join(map(str,atom)))
