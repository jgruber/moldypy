import numpy as np

BCC = np.array( [ [-0.5, 0.5, 0.5], [0.5, -0.5, 0.5], [0.5, 0.5, -0.5] ] )
FCC = np.array( [[1, 1, 0], [1, 0, 1], [0, 1, 1]] )
