from moldypy.atomistic_state import AtomisticState

a = AtomisticState.read_lammps_dump("../test/data/dump.Pt.5nm")

nnl = a.calculate_nearest_neighbor_list(4.2)
