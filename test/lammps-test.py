import moldypy.lammps as lmp
import os

def test_lammps_read_dump():
  atoms, meta = lmp.read_dump("data/lammps/dump.ortho.single")
  assert(atoms.shape == (meta['n_atoms'], len(meta['columns'])))
  assert(meta['box'][0].shape == (3,2))

  atoms, meta = lmp.read_dump("data/lammps/dump.rhombo.single")
  assert(atoms.shape == (meta['n_atoms'], len(meta['columns'])))
  assert(meta['box'][0].shape == (3,3))

def test_lammps_write_dump():
  atoms, meta = lmp.read_dump("data/lammps/dump.ortho.single")
  lmp.write_dump("dump.test", atoms, meta)
  assert(os.path.exists("dump.test"))
  os.system("rm dump.test")

if __name__ == "__main__":
  test_lammps_read_dump()
  test_lammps_write_dump()

  dump = lmp.load_dump("data/lammps/dump.ortho.single")
