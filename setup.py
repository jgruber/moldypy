from setuptools import setup, Extension
import numpy as np
import pkgconfig
 

glib_include_dirs = [s[2:] for s in pkgconfig.cflags('glib-2.0').split()]
_moldypy = Extension('_moldypy',
    define_macros = [('MAJOR_VERSION', '0'),
                     ('MINOR_VERSION', '1')],
    include_dirs = glib_include_dirs + [np.get_include()],
    library_dirs = [pkgconfig.libs('glib-2.0')],
    libraries = ['glib-2.0'],
    sources = ['_moldypy/_moldypy.c','_moldypy/nnl.c','_moldypy/util.c']
)

setup(
    name = 'moldypy',
    packages = ['moldypy'],
    version = '0.0.1',
    description = 'Tool Kit for Molecular Dynamics',
    author='Jacob Gruber',
    author_email='jacob.wb.gruber@gmail.com',
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: OS Independent',
        'Development Status :: 1 - Planning',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Physics'
    ],
    ext_modules = [_moldypy],
)