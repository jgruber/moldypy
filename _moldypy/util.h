#ifndef MOLDYPY_UTIL_H
#define MOLDYPY_UTIL_H

#include <Python.h>
#include <numpy/arrayobject.h>

/* Return the sign of a double */
static inline double sign(double x){
  return (x > 0) - (x < 0);
}

/* Modulo such that -1 % n = n - 1 */
static inline int positive_modulo(int i, int n) {
    return (i % n + n) % n;
}

static inline double NPAO_GetItem2_Float(PyArrayObject *arr, int i, int j){
  PyObject *e = PyArray_GETITEM(arr, PyArray_GETPTR2(arr, i, j));
  return PyFloat_AsDouble(e);
}

static inline double NPAO_GetItem1_Int(PyArrayObject *arr, int i){
  PyObject *e = PyArray_GETITEM(arr, PyArray_GETPTR1(arr, i));
  return (int) PyInt_AsLong(e);
}

#endif