#include "nnl.h"

/* Load various input paramters into a dump_info_t struct
 * Warning: currently assumes full PBC
 */
PyObject *
init_dump_info_py(PyObject *self, PyObject *args){
  PyArrayObject *atoms, *box_matrix, *box_boundary;
  if (!PyArg_ParseTuple(args, "OOO", &atoms, &box_matrix, &box_boundary)) {
      return NULL;
  }
  dump_info_t *data = init_dump_info(atoms, box_matrix, box_boundary);
  PyObject *capsule = PyCapsule_New((void *)data, DUMP_INFO_T_CAPSULE, free_dump_info_capsule);
  return capsule;
}


dump_info_t *
init_dump_info(PyArrayObject *atoms, PyArrayObject *box_matrix, PyArrayObject *box_boundary) {
  PyObject *e;
  box_info_t *box;
  dump_info_t *data;

  box = malloc(sizeof *box);
  data = malloc(sizeof *data);

  /* Load box */
  box->xlo = NPAO_GetItem2_Float(box_matrix, 0, 0);
  box->xhi = NPAO_GetItem2_Float(box_matrix, 0, 1);
  box->ylo = NPAO_GetItem2_Float(box_matrix, 1, 0);
  box->yhi = NPAO_GetItem2_Float(box_matrix, 1, 1);
  box->zlo = NPAO_GetItem2_Float(box_matrix, 2, 0);
  box->zhi = NPAO_GetItem2_Float(box_matrix, 2, 1);
  
  printf("0.0 %f %f %f %f %f %f\n", box->xlo, box->xhi, box->ylo, box->yhi, box->zlo, box->zhi);
  box->xperiodic = NPAO_GetItem1_Int(box_boundary, 0);
  box->yperiodic = NPAO_GetItem1_Int(box_boundary, 1);
  box->zperiodic = NPAO_GetItem1_Int(box_boundary, 2);
  printf("0.1 %d %d %d\n", box->xperiodic, box->yperiodic, box->zperiodic);
  box->lx = box->xhi - box->xlo;
  box->ly = box->yhi - box->ylo;
  box->lz = box->zhi - box->zlo;

  printf("1\n");
  data->n_atoms = PyArray_DIM(atoms, 0);
  printf("2 %d\n", data->n_atoms);
  data->box = box;
  data->atoms = atoms;

  position_t r;
  get_position(data, 30286, &r);
  printf("%f %f %f\n",  r.x, r.y, r.z);

  return data;
}

/* Free a dump_info_t */
int 
free_dump_info(dump_info_t *data) {
  free(data->box);
  free(data);
  return 0;
}

/* Free a dump_info_t capsule*/
void 
free_dump_info_capsule(PyObject *capsule) {
  dump_info_t *data = (dump_info_t *)PyCapsule_GetPointer(capsule, DUMP_INFO_T_CAPSULE);
  free_dump_info(data);
}

/* Create the nearest neighbor list using a binning method. 
 * Bin sizes are chosen based no the cutoff.
 * returns a pointer to the nnl: a 1D array of GLists.
 */ 
GList **
nnl_glib(
  dump_info_t *data,
  double cutoff) {
  
  int i, j, k, l, bin_i, bin_j, bin_k, cell_index1, cell_index2;
  int nx, ny, nz;
  double dx, dy, dz, cutoff_square;
  position_t r;
  GList **cells;
  GList **nnl;

  /* Calculate bin grid */
  nx = (int) floor( (data->box->xhi - data->box->xlo) / cutoff);
  ny = (int) floor( (data->box->yhi - data->box->ylo) / cutoff);
  nz = (int) floor( (data->box->zhi - data->box->zlo) / cutoff);
  printf("%d x %d x %d bins\n", nx ,ny ,nz);

  /* Grid Spacings */
  dx = (data->box->xhi - data->box->xlo) / nx;
  dy = (data->box->yhi - data->box->ylo) / ny;
  dz = (data->box->zhi - data->box->zlo) / nz;

  /* Initialize bins */
  cells = malloc(nx * ny * nz * sizeof *cells);
  for (i = 0; i < nx*ny*nz; i++) {
    cells[i] = NULL;
  }

  
  /* Initialize NNL */
  nnl = malloc(data->n_atoms * sizeof *nnl);
  for (i = 0; i < data->n_atoms; i++) {
    nnl[i] = NULL;
  }

  printf("Binning...\n");

  /* Bin particles */
  for (i = 0; i < data->n_atoms; i++) {
    get_position(data, i, &r);
    bin_i = (int) floor((r.x - data->box->xlo) / dx);
    bin_j = (int) floor((r.y - data->box->ylo) / dy);
    bin_k = (int) floor((r.z - data->box->zlo) / dz);
    //printf("%d -> %d %d %d (%d)\n", i, bin_i, bin_j, bin_k, j);
    j = get_cell_index(nx, ny, nz, bin_i, bin_j, bin_k);
    // printf("%d\n", j);
    cells[j] = g_list_prepend(cells[j], GINT_TO_POINTER(i));
    
  }

  printf("Checking Neighbors...\n");

  cutoff_square = cutoff*cutoff;

  l = 0;
  int cell_neighbors[27][3];
  for (i = -1; i <= 1; i++ ) {
    for (j = -1; j <= 1; j++ ) {
      for (k = -1; k <= 1; k++ ) {
        cell_neighbors[l][0] = i;
        cell_neighbors[l][1] = j;
        cell_neighbors[l][2] = k;
        l++;
      } 
    }
  }

  /* Perform neighbor checks for each cell */
  for (k = 0; k < nz; k++) {
    printf("%d\n", k);
    for (j = 0; j < ny; j++) {
      for (i= 0; i < nx; i++) {
        cell_index1 = get_cell_index(nx,ny,nz,i,j,k);

        for (l = 0; l < 27; l++){
          cell_index2 = get_cell_index(nx,ny,nz,
          positive_modulo(i+cell_neighbors[l][0],nx),
          positive_modulo(j+cell_neighbors[l][1],ny),
          positive_modulo(k+cell_neighbors[l][2],nz));
          check_cells(data, nnl, cells[cell_index1], cells[cell_index2], cutoff_square);
        }

      }
    }
  }

  /* Free bins */
  for (i = 0; i < nx*ny*nz; i++) {
    g_list_free(cells[i]);
  }

  return nnl;
}

/* Calculate distace squared between two postions */
double 
distance_square(position_t *r1, position_t *r2, box_info_t *box) {
  double dx, dy, dz;
  dx = (r2->x - r1->x);
  dy = (r2->y - r1->y);
  dz = (r2->z - r1->z);
  // printf("%3.2f %3.2f %3.2f (%3.2f %3.2f %3.2f)\n", dx, dy, dz, lx, ly, lz);
  if (box->xperiodic && fabs(dx) > 0.5*box->lx) dx -= sign(dx) * box->lx;
  if (box->yperiodic && fabs(dy) > 0.5*box->ly) dy -= sign(dy) * box->ly;
  if (box->zperiodic && fabs(dz) > 0.5*box->lz) dz -= sign(dz) * box->lz;
  return (dx*dx + dy*dy + dz*dz);
}

/* Check if two indices in the dump are neighbors
 */
short
is_neighbor(dump_info_t *data, int i1, int i2, double cutoff_square) {
  short result;
  position_t r1, r2;
  double d2;
  get_position(data, i1, &r1);
  get_position(data, i2, &r2);
  d2 = distance_square(&r1,&r2,data->box);
  result =  d2 < cutoff_square;
  return result;
}

/* Check for neighbors between two bins.
 */
void 
check_cells(dump_info_t *data, GList **nnl, GList *cell1, GList *cell2, double cutoff_square) {
  GList *p1, *p2;

  /* Do= nothing if either bin is empty */
  if (cell1 == NULL || cell2 == NULL) 
    return;

  /* Same bin */
  if (cell1 == cell2) {
    p1 = cell1->next;
    while (p1 != NULL) {
      p2 = cell1;
      while (p1 != p2) {
        resolve_neighbor(data, nnl, p1, p2, cutoff_square);
        p2 = p2->next; 
      }
      p1 = p1-> next;    }

  /* Different bins */
  } else {
    p1 = cell1;
    while (p1 != NULL) {
      p2 = cell2;
      while (p2 != NULL) {
        resolve_neighbor(data, nnl, p1, p2, cutoff_square);
        p2 = p2->next; 
      }
      p1 = p1->next;
    }
  }
}

/* Check if two GList elemeents are neighbors.
 * If they are, update the NNL.
 */
void
resolve_neighbor(dump_info_t *data, GList **nnl, GList *p1, GList *p2, double cutoff_square) {
  int i1, i2;
  short neighbor_test;
  i1 = GPOINTER_TO_INT(p1->data);
  i2 = GPOINTER_TO_INT(p2->data);
  neighbor_test = is_neighbor(data, i1, i2, cutoff_square);
  if (neighbor_test) {
    nnl[i1] = g_list_prepend(nnl[i1], p2->data);
    nnl[i2] = g_list_prepend(nnl[i2], p1->data);
  }
}

/* Converys GList of GLists to PyList of PySets */
PyObject *
nnl_glib_pylist(dump_info_t *data, double cutoff) {
  int i;
  long n = 0;
  GList *p;

  GList **nnl = nnl_glib(data, cutoff);

  PyObject *pynnl, *neighbors, *neighbor;
  PyGILState_STATE gstate = PyGILState_Ensure();
  pynnl = PyList_New(0);
  for (i = 0; i < data->n_atoms; i++) {
    //printf("C1 %d\n", i);
    neighbors = PySet_New(NULL);
    //printf("C1.5\n");
    p = nnl[i];
    //printf("C2\n");
    while(p != NULL) {
      n = (long) GPOINTER_TO_INT(p->data);
      //printf(" %d\n", (int) n);
      neighbor = PyInt_FromLong( n );
      PySet_Add(neighbors, neighbor);
      Py_DECREF(neighbor);
      p = p->next;
    }
    //printf("C3\n");
    PyList_Append(pynnl, neighbors);
    Py_DECREF(neighbors);
    //printf("C4\n");
  }
  PyGILState_Release(gstate);;
  return pynnl;
}

bin_info_t *
bin_atoms(dump_info_t *data, double cutoff){
  int i, j, k, ii, jj;
  bin_info_t *bins = malloc(sizeof *bins);
  
  /* Calculate bin grid */
  bins->nx = (int) floor( (data->box->xhi - data->box->xlo) / cutoff);
  bins->ny = (int) floor( (data->box->yhi - data->box->ylo) / cutoff);
  bins->nz = (int) floor( (data->box->zhi - data->box->zlo) / cutoff);

  /* Grid Spacings */
  bins->dx = (data->box->xhi - data->box->xlo) / bins->nx;
  bins->dy = (data->box->yhi - data->box->ylo) / bins->ny;
  bins->dz = (data->box->zhi - data->box->zlo) / bins->nz;

  /* Initialize bins */
  bins->cells = malloc(bins->nx * bins->ny * bins->nz * sizeof *bins->cells);
  for (i = 0; i < bins->nx * bins->ny * bins->nz; i++) {
    bins->cells[i] = NULL;
  }

  /* Bin particles */
  for (ii = 0; ii < data->n_atoms; ii++) {
    position_t r;
    get_position(data, ii, &r);
    i = (int) floor((r.x - data->box->xlo) / bins->dx);
    j = (int) floor((r.y - data->box->ylo) / bins->dy);
    k = (int) floor((r.z - data->box->zlo) / bins->dz);
    jj = get_cell_index(bins->nx, bins->ny, bins->nz, i, j, k);
    // printf("%d\n", j);
    bins->cells[jj] = g_list_prepend(bins->cells[jj], GINT_TO_POINTER(ii));
  }

  return bins;
}

void
free_bins(bin_info_t *bins) {
  int i;
  int n = bins->nx * bins->ny * bins->nz;
  for (i = 0; i < n; i++) {
    g_list_free(bins->cells[i]);
  }
  free(bins);
}

PyObject *
nnl_pylist_wrapper(PyObject *self, PyObject *args) {
  PyObject *dump_info_capsule;
  double cutoff;
  if (!PyArg_ParseTuple(args, "Od", &dump_info_capsule, &cutoff)) {
      return NULL;
  }
  dump_info_t *data = (dump_info_t *)PyCapsule_GetPointer(dump_info_capsule, DUMP_INFO_T_CAPSULE);
  return nnl_pylist(data, cutoff);
}

PyObject *
nnl_pylist(dump_info_t *dump, double cutoff) {
  int i, j, k, l, cell_index1, cell_index2;
  int cell_neighbors[27][3];
  bin_info_t *bins;
  PyObject *nnl, *neighbor_set;
  double cutoff_square;

  bins = bin_atoms(dump, cutoff);

  position_t r;
  get_position(dump, 30286, &r);
  printf("%f %f %f\n",  r.x, r.y, r.z);

  /* Initialize NNL */
  printf("%d Atoms\n", dump->n_atoms);
  nnl = PyList_New(dump->n_atoms);

  for (i = 0; i < dump->n_atoms; i++) {
    neighbor_set = PySet_New(NULL);
    PyList_SetItem( nnl, i, neighbor_set );
    //Py_DECREF(neighbor_set);
  }

  l = PyList_Size(nnl);
  printf("%d Atom in NNL\n", l);



  printf("Checking Neighbors...\n");

  cutoff_square = cutoff*cutoff;

  /* Adjacency deltas */
  l = 0;
  for (i = -1; i <= 1; i++ ) {
    for (j = -1; j <= 1; j++ ) {
      for (k = -1; k <= 1; k++ ) {
        cell_neighbors[l][0] = i;
        cell_neighbors[l][1] = j;
        cell_neighbors[l][2] = k;
        l++;
      } 
    }
  }

  /* Perform neighbor checks for each cell */
  for (k = 0; k < bins->nz; k++) {
    printf("%d\n", k);
    for (j = 0; j < bins->ny; j++) {
      for (i= 0; i < bins->nx; i++) {
        cell_index1 = get_cell_index(bins->nx,bins->ny,bins->nz,i,j,k);
        for (l = 0; l < 27; l++){
          cell_index2 = get_cell_index(bins->nx,bins->ny,bins->nz,
          positive_modulo(i+cell_neighbors[l][0],bins->nx),
          positive_modulo(j+cell_neighbors[l][1],bins->ny),
          positive_modulo(k+cell_neighbors[l][2],bins->nz));
          check_cells_pylist(dump, nnl, bins->cells[cell_index1], bins->cells[cell_index2], cutoff_square);
        }
      }
    }
  }

  free_bins(bins);

  return nnl;

  
}


/* Check for neighbors between two bins.
 */
void 
check_cells_pylist(dump_info_t *data, PyObject *nnl, GList *cell1, GList *cell2, double cutoff_square) {
  int i1, i2;
  GList *p1, *p2;

  /* Do= nothing if either bin is empty */
  if (cell1 == NULL || cell2 == NULL) 
    return;

  /* Same bin */
  if (cell1 == cell2) {
    p1 = cell1->next;
    while (p1 != NULL) {
      p2 = cell1;
      while (p1 != p2) {
        i1 = GPOINTER_TO_INT(p1->data);
        i2 = GPOINTER_TO_INT(p2->data);
        resolve_neighbor_pyset(data, nnl, i1, i2, cutoff_square);
        p2 = p2->next; 
      }
      p1 = p1-> next;    }

  /* Different bins */
  } else {
    p1 = cell1;
    while (p1 != NULL) {
      p2 = cell2;
      while (p2 != NULL) {
        i1 = GPOINTER_TO_INT(p1->data);
        i2 = GPOINTER_TO_INT(p2->data);
        resolve_neighbor_pyset(data, nnl, i1, i2, cutoff_square);
        p2 = p2->next; 
      }
      p1 = p1->next;
    }
  }
}

/* Check if two GList elemeents are neighbors.
 * If they are, update the NNL.
 */
void
resolve_neighbor_pyset(dump_info_t *data, PyObject *nnl, int i1, int i2, double cutoff_square) {
  int n;
  PyObject *neighbor, *neighbor_set;

  if (is_neighbor(data, i1, i2, cutoff_square)) {
    n = GPOINTER_TO_INT(i2);
    neighbor = PyInt_FromLong( (long) n );
    neighbor_set = PyList_GetItem(nnl, i1);
    if (neighbor_set == NULL) {
      // Raise exception!
    }
    PySet_Add(neighbor_set, neighbor);
    //Py_DECREF(neighbor);
 
    n = GPOINTER_TO_INT(i1);
    neighbor = PyInt_FromLong( (long) n );
    PySet_Add(PyList_GetItem(nnl, i2), neighbor);
    //Py_DECREF(neighbor);
  }
}


































