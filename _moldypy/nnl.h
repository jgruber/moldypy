#ifndef MOLDYPY_NNL_H
#define MOLDYPY_NNL_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/queue.h>
#include <math.h>
#include <glib.h>
#include <Python.h>
#include <numpy/arrayobject.h>
#include "util.h"

#define DUMP_INFO_T_CAPSULE "_moldypy.dump_info_t"

/* Utility class for atom positions */
typedef struct {
  double x,y,z;
} position_t;

/* Contains info about the simulation ceell */
typedef struct {
  double xlo, xhi, ylo, yhi, zlo, zhi, lx, ly, lz;
  int xperiodic, yperiodic, zperiodic;
} box_info_t;

/* Contains info about the dump file: atoms, stride and simulation cell */
// typedef struct {
//   int n_atoms;
//   double *atom_positions;
//   int stride;
//   box_info_t *box;
// } dump_info_t;

typedef struct {
  int n_atoms;
  PyArrayObject *atoms;
  box_info_t *box;
} dump_info_t;


/* info aabout the binning */
typedef struct {
	int nx,ny,nz;
	double dx,dy,dz;
  GList **cells;
} bin_info_t;


static inline position_t *
get_position(dump_info_t *data, int index, position_t *p) {
  p->x = NPAO_GetItem2_Float(data->atoms, index, 0);
  p->y = NPAO_GetItem2_Float(data->atoms, index, 1);
  p->z = NPAO_GetItem2_Float(data->atoms, index, 2);
  return p;
}

static inline int 
get_cell_index(int nx, int ny, int nz, int i, int j, int k) {
  return i + nx*j + nx*ny*k;
}

// dump_info_t *
// init_dump_info(PyObject *atoms, const double *box_matrix, PyObject *box_boundary); 

dump_info_t *
init_dump_info(PyArrayObject *atoms, PyArrayObject *box_matrix, PyArrayObject *box_boundary);

PyObject *
init_dump_info_py(PyObject *self, PyObject *args);

int 
free_dump_info(dump_info_t *data);

void
free_dump_info_capsule(PyObject *capsule);

GList ** 
nearest_neighbor_list(
  int n_atoms, 
  double *atom_positions,
  const int *strides,
  const double *box_in,
  double cutoff);

/* Calculate distace squared between two postions */
double 
distance_square(position_t *r1, position_t *r2, box_info_t *box);

short 
is_neighbor(dump_info_t *data, int i1, int i2, double cutoff_square);

/* Check for neighbors between two bins.
 */
void
check_cells(dump_info_t *data, GList **nnl, GList *cell1, GList *cell2, double cutoff_square);


/* Check if two GList elemeents are neighbors.
 * If they are, update the NNL.
 */
void
resolve_neighbor(dump_info_t *data, GList **nnl, GList *p1, GList *p2, double cutoff_square);

/* Converts GList of GLists to PyList of PySets */
PyObject *
nnl_to_py(GList **nnl, int n_atoms);

PyObject *
nnl_glib_pylist(dump_info_t *data, double cutoff);

PyObject *
nnl_pylist_wrapper(PyObject *self, PyObject *args);

PyObject *
nnl_pylist(dump_info_t *dump, double cutoff);

/* Check if two GList elemeents are neighbors.
 * If they are, update the NNL.
 */
void
resolve_neighbor_pyset(dump_info_t *data, PyObject *nnl, int i1, int i2, double cutoff_square);

/* Check for neighbors between two bins.
 */
void 
check_cells_pylist(dump_info_t *data, PyObject *nnl, GList *cell1, GList *cell2, double cutoff_square);

#endif