#include <Python.h>
#include <numpy/arrayobject.h>
#include "nnl.h"

// PyDoc_STRVAR(moldypy_module_doc,
// "Nearest Neighbor List Calculation\n"
// "\n"
// "ToDo: Finish Docs\n");

static struct PyMethodDef moldypy_methods[] = {
    { "nnl_glib_pylist", (PyCFunction)nnl_glib_pylist,
        METH_VARARGS, NULL},
    { "nnl_pylist", (PyCFunction)nnl_pylist_wrapper,
        METH_VARARGS, NULL},
    { "init_dump_info", (PyCFunction)init_dump_info_py,
        METH_VARARGS, NULL},
    { NULL, NULL }
};


PyMODINIT_FUNC
init_moldypy(void) {
  PyObject *m;

  m = Py_InitModule("_moldypy", moldypy_methods);
  import_array();

}