# moldypy

moldypy is a python toolkit for molecular dynamics. It is designed to be
lightweight and flexible while efficiently and robustly implementing common
algorithms.

## Features

 - Read and write LAMMPS dump files
 - Utility functions including:
 - Calculate nearest neighbor list
 - Continuum Metrics

## Getting Started

moldypy depends on:
1. C libraries:
  - glibc
2. Python packages
 - pkg-config
 - numpy
 - pandas

Once you have these, install with
```
pip install -e /path/to/moldypy
```
